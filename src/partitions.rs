use gpt::disk;
use gpt::partition::Partition;
use std::collections::BTreeMap;
use std::io;
use std::path::{Path, PathBuf};

fn get_partitions_for_dev(dev_path: &Path) -> Result<BTreeMap<u32, Partition>, io::Error> {
    let size = disk::LogicalBlockSize::Lb4096;
    let header = gpt::header::read_header(dev_path, size);
    match header {
        Ok(header) => gpt::partition::read_partitions(dev_path, &header, size),
        Err(e) => { println!("Invalid device {:?}, returned error: {} -- SKIPPING", dev_path, e); Ok(BTreeMap::new()) },
    }
}

pub fn get_slot_partitions_device(dev_path: &PathBuf) -> (Vec<(Partition, PathBuf)>, Vec<(Partition, PathBuf)>) {
    let all_partitions = get_partitions_for_dev(dev_path.as_path()).unwrap();
    let mut a_parts: Vec<(Partition, PathBuf)> = Vec::new();
    let mut b_parts: Vec<(Partition, PathBuf)> = Vec::new();

    for i in all_partitions {
        //Search for slotted partitions
        let name = i.1.name.clone();
        let len = name.len();
        let name_end = &name[len-2..];
        if name_end.eq("_a") {
            a_parts.push((i.1, dev_path.to_path_buf()));
        }
        else if name_end.eq("_b") {
            b_parts.push((i.1, dev_path.to_path_buf()));
        }
    }

    return (a_parts, b_parts);
}

pub fn get_slot_partitions(devs: Vec<PathBuf>) -> (Vec<(Partition, PathBuf)>, Vec<(Partition, PathBuf)>) {
    //Open relevant GPT stuff
    let blockdevs: Vec<PathBuf>;
    if devs.is_empty() {
        blockdevs = block_utils::get_block_devices().unwrap();
    }
    else {
        blockdevs = devs.clone();
    }

    let mut a_parts: Vec<(Partition, PathBuf)> = Vec::new();
    let mut b_parts: Vec<(Partition, PathBuf)> = Vec::new();

    for i in blockdevs {
        //Iterates over block devices and adds slotted partitions
        let (a_ext, b_ext) = get_slot_partitions_device(&i);
        a_parts.extend(a_ext);
        b_parts.extend(b_ext);
    }

    return (a_parts, b_parts);
}

pub fn get_boot_partitions(devs: Vec<PathBuf>) -> (Partition, Partition, PathBuf) {
    let (a_parts, b_parts) = get_slot_partitions(devs);
    let mut boot_a: Option<Partition> = None;
    let mut boot_b: Option<Partition> = None;
    let mut path: Option<PathBuf> = None;
    a_parts.into_iter().for_each(|i| {
        if i.0.name.eq("boot_a") {
            boot_a = Some(i.0);
            path = Some(i.1);
        }
    });
    b_parts.into_iter().for_each(|i| {
        if i.0.name.eq("boot_b") {
            boot_b = Some(i.0);
        }
    });

    //Returns boot partitions specifically
    return (boot_a.unwrap(), boot_b.unwrap(), path.unwrap());
}
