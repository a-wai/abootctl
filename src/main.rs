use clap::{App, Arg};
use modular_bitfield::prelude::*;
use std::path::PathBuf;

mod partitions;

#[cfg(test)]
mod tests;

#[bitfield(bits = 64)]
#[derive(Debug, Clone)]
pub struct SlotInfo {
    #[skip]
    __: B50,
    is_active: B1, // Bit 50
    #[skip]
    __: B13
}

pub trait BootSlot {
    fn from_boot_flags(flags: u64) -> SlotInfo;
    fn get_boot_flags(&self) -> u64;
}

impl BootSlot for SlotInfo {
    fn from_boot_flags(flags: u64) -> SlotInfo {
        let slot = SlotInfo::from_bytes(flags.to_le_bytes());
        return slot;
    }

    fn get_boot_flags(&self) -> u64 {
        return u64::from_le_bytes(self.clone().into_bytes());
    }
}

fn main() {
    // CLI stuff
    let matches = App::new("abootctl")
        .version("1.0.0")
        .author("Aissa Z. B. <aissa.zenaida@pm.me>, Caleb C. <caleb@connolly.tech>")
        .about("Bootloader control for SDM845 OnePlus devices.")
        .arg(Arg::with_name("DEV_DIR")
            .required(true)
            .help("Operate on image files within the target directory instead of operating on block devices"))
        .get_matches();

    let custom_devpath = PathBuf::from(matches.value_of("DEV_DIR").unwrap_or(""));
    let mut devs: Vec<PathBuf> = Vec::new();
    devs.push(custom_devpath);

    get_current_slot(devs);
}

fn get_slot_info(devs: Vec<PathBuf>) -> (SlotInfo, SlotInfo) {
    let (boot_a, boot_b, _) = partitions::get_boot_partitions(devs);
    let slot_a = SlotInfo::from_boot_flags(boot_a.flags);
    let slot_b = SlotInfo::from_boot_flags(boot_b.flags);
    return (slot_a, slot_b);
}

fn get_current_slot(devs: Vec<PathBuf>) {
    let (slot_a, slot_b) = get_slot_info(devs);
    if (slot_a.is_active() == 1) && (slot_b.is_active() == 0) {
        println!("a");
    } else if (slot_a.is_active() == 0) && (slot_b.is_active() == 1) {
        println!("b");
    } else {
        panic!("Corrupted headers; none or both partitions marked active: {:#b}, {:#b}", slot_a.get_boot_flags(), slot_b.get_boot_flags());
    }
}
