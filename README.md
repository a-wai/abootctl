# sdm845-abootctl
Utility to control boot slots on OnePlus SDM845 devices from mainline Linux by flipping the relevant bits in the GPT partition table flags. Heavily WIP, but barebones functionality should be present soon.

WARNING: DO NOT USE YET. THIS MAY BRICK YOUR DEVICE, WIPE YOUR PARTITION TABLE, OR AWAKEN THE GREAT OLD ONES OF R'LYEH. NO MATTER WHAT THIS UTILITY CAUSES, MATERIAL OR ESOTERIC, YOU HAVE BEEN WARNED.
```
USAGE:
    sdm845-abootctl [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    hal-info                     Show info about boot_control HAL used
    get-number-slots             Prints number of slots
    get-current-slot             Prints currently running SLOT
    mark-boot-successful         Mark current slot as GOOD
    set-active-boot-slot         On next boot, load and execute SLOT
    set-slot-as-unbootable       Mark SLOT as invalid
    is-slot-bootable             Returns 0 only if SLOT is bootable
    is-slot-marked-successful    Returns 0 only if SLOT is marked GOOD
    get-suffix                   Prints suffix for SLOT
    help                         Prints this message or the help of the given subcommand(s)
```
Written by Aissa Z. B. <aissa.zenaida@pm.me> and Caleb C. <caleb@connolly.tech>

## Building

To build on postmarketOS (or Alpine) you will need to install the `eudev-dev` and `cargo` packages.

You can then build with
```sh
cargo build
```

And run the resulting binary `target/debug/sdm845-abootctl` as root.

# Docs

Qualcomm slot management is, *interesting*... **Every** partition that is "slotted" (has more than one copy with slots) has it's own copy of the flags.

The slot status flags are encoded in the GPT partition header attribute flags field, [more info here](https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_entries_(LBA_2%E2%80%9333)). Bits 48-64 of the field are left for partition specific use, in the case of every "slotted" partition, they are mapped as follows:
```rust
// Simplified for brevity
struct SlotInfo {
    __: B48,

    priority: B2, // Bits 48,49
    is_active: B1, // Bit 50
    retry_count: B3, // Bits 51,52,53
    boot_successful: B1, // Bit 54
    is_unbootable: B1,   // Bit 55

    __: B8
}
```

It seems somewhat counter intuitive, but yes this means that when you switch from slot A to slot B the `is_active` flag on every single "slotted" partition is modified.

In addition, the GUIDs are specific to the active partition, upon changing the active partition the GUIDs must all be swapped, e.g. the GUIDs for the `boot_a` partition and `boot_b` partition get swapped. This is presumably used by Android and is enforced by the bootloader.